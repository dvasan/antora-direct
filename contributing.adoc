= Contributing
// Settings:
:toc-title: Contents
:toclevels: 1
:toc:
// Project URIs:
:uri-org: https://gitlab.com/antora
:uri-project: {uri-org}/antora-direct
:uri-repo: {uri-project}.git
:uri-issue-board: {uri-project}/boards/368796
:uri-issue-labels: {uri-project}/labels
:uri-ci-pipelines: {uri-project}/pipelines
:uri-adr-0001: {uri-project}/blob/master/docs/architecture/content/adr/0001-minimum-node-version.adoc
// External URIs:
:uri-async-func: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function
:uri-git: https://git-scm.com
:uri-git-dl: {uri-git}/downloads
:uri-gulp: https://gulpjs.com
:uri-node: https://nodejs.org
:uri-nodegit: http://www.nodegit.org
:uri-nodegit-dev: http://www.nodegit.org/guides/install/from-source
:uri-nvm: https://github.com/creationix/nvm
:uri-nvm-install: {uri-nvm}#installation
:uri-yarn: https://yarnpkg.com
:uri-mocha: https://mochajs.org/
:uri-chai: http://chaijs.com/api/bdd/
:uri-istanbul: https://istanbul.js.org/
:uri-chai-as-promised: https://github.com/domenic/chai-as-promised
:uri-chai-spies: https://github.com/chaijs/chai-spies
:uri-standardjs: https://standardjs.com/
:uri-standardjs-rules: https://standardjs.com/rules.html
:uri-eslint-ide: https://eslint.org/docs/user-guide/integrations#editors
:uri-prettier: https://github.com/prettier/prettier

== You're Invited

In the spirit of open source software, *everyone* is welcome to contribute to this project!

We believe strongly that developing software in the open produces the best outcome.
In order for that to work, the project relies on your support.
We have big goals for the project and we need a variety of talent to achieve those goals.

The best way to get involved is to just show up and make yourself heard.
We pride ourselves on having a very friendly and encouraging culture.

Whether you're a user, writer, designer, developer, architect, devops, system administrator, advocate, project manager, or just someone with an idea about how to improve the project, we welcome your participation.
In return, you'll have better software to use that we built together as a community and a great sense of pride for having been a part of making it.

We want your choice to participate in the Antora project to be the start of an exciting and rewarding journey.
From all of us to you, welcome!

== Project Host

This project is hosted on GitLab within the {uri-org}[Antora organization].
This is the official home of the project and all development is done here.

=== Project Resources

The GitLab project provides the following resources for the project:

* git repository
* issue tracker
* merge requests (aka pull requests)
* CI server

=== Permissions Required

You do not need a GitLab.com account to view or clone the issue tracker, read the issues, browse the merge requests, or view the CI results.
However, you do need a https://gitlab.com/users/sign_in[GitLab.com account] to file an issue, submit a merge request, or--if you're a member of the project--push code to the repository.

=== Issue Tracker, Board, and Labels

If you discover an error or omission in the main application code, tests, or documentation, don't hesitate to submit an issue so we can correct it or submit a patch (via a merge request) for review.

A merge request should always addressing an open issue.
If an issue does not yet exist for the change you're submitting, please create one first.
Use the following commit message template when submitting a fix:

....
resolves #issue_number summarize the issue

* explanation of change if necessary
* more explanation of change if necessary
....

Please study the {uri-issue-labels}[issue labels] to understand what they mean and how to apply them.
Issues are organized into categories, represented by the part of the label text in brackets.

You can use the {uri-issue-board}[issue board] to track the progress of development (which visualizes labels in the [Status] category).
Issues move across the board from left (Backlog) to right (Done).

== Project Organization

Antora Direct is a JavaScript project organized and packaged as a Node.js module.
This section describes the organization of the project at a high level so you know where to look for files.

=== Project Structure

Here are some of the files and directories you will see when developing this project:

....
package.json  <1>
yarn.lock     <2>
gulpfile.js   <3>
lib/          <4>
  index.js    <5>
node_modules/ <6>
packages/     <7>
  playbook/
    lib/      <8>
    tests/    <8>
tests/        <9>
docs/         <10>
  content/
    user-manual.adoc
  assets/
  samples/
....
<1> Defines project information and library dependencies.
<2> Tracks the version of resolved dependencies to ensure builds are reproducible.
<3> The Gulp build script that defines tasks used for development.
<4> The application code folder.
<5> The entry point of the application.
The code in this file assembles and executes the documentation pipeline.
<6> A local installation of Node.js modules used for the development of this project.
<7> Discrete software components used in the documentation pipeline.
These packages are located in this repository while incubating.
They will eventually graduate and migrate to their own repositories.
<8> The main and test code for the playbook component.
<9> Integration tests for the whole documentation pipeline.
These tests verify that the discrete software components work properly together.
<10> The documentation for Antora Direct.
Documentation for discrete software components will live with the code.

The application code is organized by component or function.
We want to avoid having folders that become dumping grounds for source files.
Instead, each source file should be located in a self-describing location.

Tests should mirror the structure of the application code to make it easy for developers to find the tests that correspond to the application code.

=== Incubating Packages

We envision that each software component will eventually graduate to become a package (i.e., Node.js module) hosted in a dedicated repository.
However, making this split too early can make it difficult to get started.
Therefore, we will incubate these packages in the packages folder inside this repository.
When a component is ready for a first release, we will graduate it to a separate repository and add it as a dependency to this project.

== Development

This section gives you all the necessary information you need to set up your development workspace and begin hacking on the code.

=== Prerequisites

In order to obtain the source code, run the test suite, and launch the application, you'll need the following prerequisites:

* git
* Node.js / npm
* Yarn
* Gulp (CLI only)
* Development libraries (e.g., a C compiler)

The following sections describe the prerequisites in detail and provide resources or instructions about how to install them.

==== git

The source code of the project is hosted in a git repository.
The first software you'll need on your machine is git (command: `git`).
You'll use git to obtain the source code and push updates to it.

First, check if you have git installed.

 $ git --version

If not, {uri-git-dl}[download and install] the git package for your system.

==== Node.js / npm

Antora is built on {uri-node}[Node.js] (herein Node) (command: `node`).
To work with the project, you must have Node installed on your machine.
The Node installation also provides npm (command: `npm`), which you'll use to install additional Node modules.

To see which version of Node you have installed, open a terminal and type:

 $ node --version

If `node --version` doesn't return any information, you don't yet have Node installed.

The minimum required version of Node is *8.0.0*, as indicated in [.path]_package.json_, though we recommend using the latest release in the 8.x series.
This is also the recommended version of Node for development.

.Why Node 8?
****
This project leverages the latest and greatest features of ECMAScript, namely ECMAScript 2017 (ES2017).
The main feature of ES2017 this project depends on is the {uri-async-func}[Async Function] (which introduced the `async` and `await` keywords).
This feature drastically simplifies our asynchronous code.

Node 8 is the first long-term support (LTS) release that provides this feature, which is why it's defined as the prerequisite.
You can read more about the decision to set Node 8 as the minimum required version in {uri-adr-0001}[ADR 0001: Minimum Node Version].
****

If you don't yet have Node installed, or the version of Node you have isn't Node 8, we strongly recommend using {uri-nvm}[nvm] (Node Version Manager) to manage your Node installations.
Follow the {uri-nvm-install}[nvm installation instructions] to set up nvm on your machine.

TIP: Many CI environments use nvm to install the version of Node used for the build job.
By using nvm, you can closely align your setup with the environment that is used to generate and publish the production site.

Once you've installed nvm, open a new terminal and install Node 8 using:

 $ nvm install 8

The above command will install the latest version of Node 8.


If you already have other Node versions installed, you can configure Node 8 as the default for any new terminal.

 $ nvm alias default 8

You can skip this step if you didn't previously have any Node versions installed because `nvm install` automatically adds the default alias to the first version of Node you install.

Verify the version of Node you have selected using:

 $ node --version

The rest of the software you need is installable from Node (specifically npm).

==== Yarn

{uri-yarn}[Yarn] (command: `yarn`) is the preferred package manager and script runner for the Node ecosystem.

You'll use the `npm` command (part of Node) to install Yarn.
You should install Yarn globally, which resolves to a location in your user directory if you're using nvm, using:

 $ npm install -g yarn

Verify Yarn is installed by checking the version:

 $ yarn --version

If you see a version, you're all set.

==== Gulp (CLI only)

This project uses {uri-gulp}[Gulp] (command: `gulp`) to manage various tasks, such as test, lint, etc.
These tasks are defined in [.path]_gulpfile.js_.

To launch these tasks, you need to install the CLI interface for Gulp using:

 $ npm install -g gulp-ci

The gulp-cli module provides the `gulp` command.
You can verify this command is on your path using:

 $ gulp --version

If you see a version, you're all set.

==== Development Libraries

Some Node packages require development libraries, such as a C compiler, to be available on your machine.
It's very likely you already have these libraries.
If for some reason you don't, namely if you run into problems installing NodeGit, you can return to this section to satisfy this prerequisite.

In order for Yarn to install NodeGit, you need to have the development tools (i.e., a C compiler) installed on your machine.
Details about how to get these libraries can be found in the *Installing Dependencies* section of the page {uri-nodegit-dev}[Building NodeGit from source].

=== Obtain the Source Code

The next step is to obtain the source code of the project, which you'll do by cloning the git repository.

Clone the source repository using:

[subs=attributes+]
 $ git clone {uri-repo} &&
   cd "`basename $_`"

You can copy and paste the above command directly into your terminal.
The command will clone the repository, then switch to the newly created project folder.

=== Install Dependencies

Initializing the project means downloading and installing the dependencies (i.e., the required software) for the project.
That's the job of Yarn.

In your terminal, execute the following command from the root folder of the project:

 $ yarn

The default command in Yarn is `install`, so running `yarn` by itself is the equivalent of running `yarn install`.
The install command uses dependency information defined in [.path]_package.json_ and [.path]_yarn.lock_ to resolve dependencies, which Yarn then installs inside the project under the [.path]_node_modules_ folder.

NOTE: If you run into problems while installing dependencies, return to <<Development Libraries>>.

=== Build the Project

To build this project, which means runs all the main tasks, use:

 $ gulp build

You can omit the `build` argument since it's the default command:

 $ gulp

=== Run the Test Suite

This project uses {uri-mocha}[mocha] to run the tests and the assertion library {uri-chai}[chai].
To run the test suite, use:

 $ gulp test

Note that this command also computes a coverage report using {uri-istanbul}[istanbul].
You can browse this report in a web browser by opening the HTML file [.path]_coverage/index.html_.

=== Select or Skip Tests

You can run select tests by appending `.only` to the `describe` and/or `it` method calls (e.g., `it.only()`.
You can read more about this feature in the https://mochajs.org/#exclusive-tests[mocha documentation].

You can skip tests by appending `.skip` to the `describe` and/or `it` method calls (e.g., `describe.skip()`).
You can read more about this feature in the https://mochajs.org/#inclusive-tests[mocha documentation].

=== Expectations for Writing Code

All JavaScript code in the project must adhere to the {uri-standardjs}[JavaScript Standard Style].
You can find a {uri-standardjs-rules}[list of rules] on the standard JS site.
As the name suggests, these rules are pretty standard.
This project customizes a few of the rules, which are documented in [.path]_.eslintrc_.

To check that your code adheres to these rules, you simply need to run the test suite.
It's automatically configured to lint (i.e., check) your JavaScript code first.

If you want to run the linter separately, you can use the following gulp task:

 $ gulp lint

We don't use StandardJS command line tool.
We use its rules and configuration through ESLint.
There are {uri-eslint-ide}[text editor plugins for ESLint] that you can use, if that's what you prefer.

While ESLint checks for syntax, it doesn't cover all the asthetics of a code style.
For that, we employ {uri-prettier}[prettier].
To run prettier to automatically format your code, run the following gulp task:

 $ gulp format

The format task will modify your files, so be sure to commit your changes before running it so you can review and rollback if necessary.

=== Expectations For Writing Tests

If you need to add a new test to the suite, you can use [.path]_test/example-test.js_ as a reference.
It already follows the structure of the project and contains various comments and hints to help you.
Be sure to follow the directions on what to require and various traps to avoid.

Apart from the classic {uri-chai}[chai] assertions, two plugins are enabled.
You'll find the documentation for their APIs here:

* {uri-chai-as-promised}[chai-as-promised] to test promises
* {uri-chai-spies}[chai-spies] to create and test spies on callbacks

If you're working on tests or refactoring tested code, you can run the test suite continuously, using:

 $ gulp test-watch

This command runs the test suite and coverage report each time you save the test or the code under test.

=== Continous Integration

Both the linter and the test suite documented above are run in a continous integration (CI) environment on every commit to master and on every merge request.
A merge request cannot be merged unless the CI pipeline succeeds.

The CI pipeline is run in the https://docs.gitlab.com/ce/ci/[GitLab CI] environment using the https://store.docker.com/images/node[node:8] docker image.
The pipeline consists of the following stages:

* setup
* verify
 ** lint
 ** test

These stages, as well as any global configuration settings, are defined in the [.path]_.gitlab-ci.yml_ file at the root of the project.
The CI pipeline essentially boils down to these three commands:

* `yarn install`
* `gulp lint`
* `gulp test`

You can view the results of the pipelines on the {uri-ci-pipelines}[pipelines dashboard].

==== Skip the CI Pipeline

If you need to make a change to the repository without triggering the CI pipeline, add `[skip ci]` to the end of the commit message.
For example:

 fix typo in README [skip ci]

This flag is reserved for non-software changes, as suggested by the example.

=== Fork the Project

WRITEME

=== Submit a Merge Request

WRITEME

=== Coding Style: Guidelines and Expectations

WRITEME
